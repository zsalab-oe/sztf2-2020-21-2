﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Abstract_Sealed_Method
{
    abstract class Jatekos // egy osztály kötelezően absztrakt, ha legalább egy absztrakt metódusa van
    {
        // mivel a Játékos ősosztályban még nem tudom eldönteni, hogy ő majd hogyan, s mi alapján fog köszönni, ezért itt ezt a metódust absztrakttá teszem
        // majd a leszármazottakban (ld. lentebb) kifejtem
        // nincs metódustörzs
        // az abstract kulcsszó egyben virtualitást is jelent

        public abstract void Bemutatkozas();
    }

    class Kapus : Jatekos
    {
        // absztrakt metódusokat a leszármazottakban kötelező implementálni (vagy absztraktként jelölni)
        public override void Bemutatkozas()
        {
            Console.WriteLine("Őrzöm a hálót.");
        }
    }


    abstract class Tamado : Jatekos
    {

    }

    class Csatar : Tamado  // Ctrl + .
    {
        public sealed override void Bemutatkozas()
        {
            Console.WriteLine("Bevarrom a léc alá.");
        }
    }

    class KetBallabasCsatar : Csatar
    {
        //public override void Bemutatkozas()
        //{
        //    Console.WriteLine("Tulajdonképpen én is csatár volnék.");
        //}
        // Hibás, lezárt metódust nem lehet felülírni
    }

    class Program
    {
        static void Main(string[] args)
        {
            Jatekos[] csapat = new Jatekos[]
            {
                new Kapus(),
                new Csatar(),
                new Csatar()
            };

            for (int i = 0; i < csapat.Length; i++)
            {
                csapat[i].Bemutatkozas();
            }

            Console.ReadLine();
        }
    }
}
