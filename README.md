# Szoftvertervezés és -fejlesztés II.
--------------

A Szoftvertervezés és -fejlesztés II. laborokon ismertetett anyagok kódjai és példái.

## Letöltés

* .zip letöltés: Downloads >> Download repository
* cli: `git clone https://zsalab-oe@bitbucket.org/zsalab/sztf2-2020-21-2.git`

## Hasznos linkek

* Az anyagok megtalálhatók a [http://users.nik.uni-obuda.hu/zsalab/sztf2/](http://users.nik.uni-obuda.hu/zsalab/sztf2/) oldalamon is.
* Előadás- és labordiák a tantárgy honlapján: [http://users.nik.uni-obuda.hu/sztf2/](http://users.nik.uni-obuda.hu/sztf2/)
* Tavalyi laboranyagok: [https://bitbucket.org/zsalab-oe/sztf2-2019-20-2/](https://bitbucket.org/zsalab-oe/sztf2-2019-20-2/)

## Kapcsolat

Kérdés, óhaj, sóhaj, panasz esetén bátran keressetek a `gaspar.balazs {at} nik.uni-obuda.hu` e-mail címen!

-----------------
*Gáspár Balázs*