﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_3_Feladat_OOP
{
    // ==============================================================================================================
    // 
    // Ebben a példában sztringek helyett Dolgozo objektumok reprezentálják a feladatokhoz beosztható embereket.
    //
    // Az algoritmus rugalmas voltát mutatja az is, hogy OOP környezetben egy absztrakt osztályként nagyon jól megvalósítható. Az ős absztrakt osztály tartalmazza az algoritmushoz szükséges mezőket, illetve magát a megoldó algoritmust. Az Ft és az Fk függvények célszerűen (virtuális) absztrakt metódusokként jelenhetnek meg, hiszen az általános alak még nem alkalmas tényleges feladatok megoldására. A későbbiekben egy konkrét feladat megoldása során elég egy leszármazottat készíteni, amely értéket ad a szükséges mezőknek, illetve a feladatnak megfelelően megvalósítja az Ft és Fk függvényeket.
    // 
    // ==============================================================================================================


    class Program
    {
        static void Main(string[] args)
        {
            int N = 6;
            Dolgozo[,] R = new Dolgozo[N, 3];

            // feladatkör 1
            R[0, 0] = new Dolgozo("Peter");
            R[0, 1] = new Dolgozo("Lois");

            // feladatkör 2
            R[1, 0] = new Dolgozo("Chris");
            R[1, 1] = new Dolgozo("Peter");

            // feladatkör 3
            R[2, 0] = new Dolgozo("Meg");

            // feladatkör 4
            R[3, 0] = new Dolgozo("Meg");
            R[3, 1] = new Dolgozo("Stewie");
            R[3, 2] = new Dolgozo("Chris");

            // feladtkör 5
            R[4, 0] = new Dolgozo("Meg");
            R[4, 1] = new Dolgozo("Brian");

            // felaatkör 6
            R[5, 0] = new Dolgozo("Brian");
            R[5, 1] = new Dolgozo("Peter");

            int[] M = new int[] { 2, 2, 1, 3, 2, 2 };
            
            Dolgozo[] E = new Dolgozo[N];

            Feladatkioszto kioszto = new Feladatkioszto(N, R, M);

            if (kioszto.MegoldasKereses(E))
            {
                for (int i = 0; i < E.Length; i++)
                {
                    Console.WriteLine("Feladatkör " + (i + 1) + ": " + E[i].Nev);
                }
            }
            else
            {
                Console.WriteLine("Nincs megoldás");
            }

            Console.ReadLine();
        }
    }
}
