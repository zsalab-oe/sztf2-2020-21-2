﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Esemenykezeles_Delegate_Event
{
    class Program
    {
        static void Main(string[] args)
        {
            // Eseménykezelés delegált / event használatával


            Lottozo lottozo = new Lottozo();

            /* 1. verzió: publikus delegált (not OK) */
            //lottozo.nyert += Ertesites;
            //lottozo.Sorsolas();
            // Probléma, ha publikus a delegált, mert ez esetben innen kívülről meghívhatom tetszőlegesen, ezáltal fals programot készítve. Pl.:
            //lottozo.nyert("Hiba, sorsolás nélkül nyertem...");



            /* 2. verzió: privát delegált + publikus le- és feliratkoztató metódusok (OK) */
            //lottozo.Feliratkozas(Ertesites);
            //lottozo.Sorsolas();



            /* 3. verzió: event használata (OK) */
            lottozo.nyert += Ertesites;
            lottozo.Sorsolas();

            // hibás, event használatával innen kívülről NEM hívhatom meg tetszőlegesen, ezzel kiküszöbölve a helytelen működést
            //lottozo.nyert("teszt");


            // event: egy korlátozott, biztonságosabb delegált
            // a mezőt tartalmazó osztályon kívülről gyakorlatilag csak fel- és leiratkoztatási célokból férhetünk hozzá (csak a +=, -= operátorok működnek)


            Console.ReadLine();
        }

        static void Ertesites(string szoveg)
        {
            Console.WriteLine(szoveg);
        }
    }
}
