﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Esemenykelzeles_Interface.Valami
{
    class Program
    {
        static void Main(string[] args)
        {
            Telefon t1 = new Telefon("1234567");
            Telefon t2 = new Telefon("6666666");

            // eseménykezelő objektum példányosítása
            HivasNaplo naplo = new HivasNaplo();

            // eseménykezelő objektum "regisztrálása”
            t1.FigyeloRegisztralas(naplo);
            t2.FigyeloRegisztralas(naplo);

            // teszt
            t1.EgyenlegFeltolt(50);
            t1.HivasKezdemenyezes(t2);

            Console.WriteLine();
            t2.HivasKezdemenyezes(t1);


            Console.ReadLine();
        }
    }
}
