﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Esemenykelzeles_Interface
{
    // "eseményt" kezelő osztály
    // Ez gyakorlatilag csak azért kell, mivel az interfészben csak előírni tudjuk a metódusokat, azokat valahol implementálni kell (meg kell adni a futtatható kódot tartalmazó metódustörzseket).
    class HivasNaplo : IHivasFigyelo
    {
        // küldő: aki az ESEMÉNYT küldte (akit épp felhívtak)
        public void BejovoHivasTortent(Telefon kuldo, string forras_telefonszam)
        {
            Console.WriteLine($"{kuldo.Telefonszam} számot hívta {forras_telefonszam}");
        }

        // küldő: aki az ESEMÉNYT küldte (aki kezdeményezte a hívást)
        public void KimenoHivasTortent(Telefon kuldo, string cel_telefonszam)
        {
            Console.WriteLine($"{kuldo.Telefonszam} hívta {cel_telefonszam} telefonszámot");
        }
    }
}
