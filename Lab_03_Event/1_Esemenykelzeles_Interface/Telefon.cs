﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Esemenykelzeles_Interface
{
    class Telefon
    {
        const int HIVASDIJ = 20;

        public string Telefonszam { get; private set; }
        int egyenleg;

        IHivasFigyelo hivasFigyelo; // referencia az eseménykezelő objektumra, ezen keresztül tudjuk hívni az eseménykezelő metódusokat

        public void FigyeloRegisztralas(IHivasFigyelo hivasFigyelo) // eseménykezelő objektum "regisztrálása”
        {
            this.hivasFigyelo = hivasFigyelo;
        }


        public Telefon(string telefonszam)
        {
            Telefonszam = telefonszam;
            egyenleg = 0;
        }

        public void EgyenlegFeltolt(int osszeg)
        {
            egyenleg += osszeg;
        }



        void HivasFogadas(Telefon forras)
        {
            if (hivasFigyelo != null)
                hivasFigyelo.BejovoHivasTortent(this, forras.Telefonszam); // jelzés az "eseményről" (történt valami)
        }

        public void HivasKezdemenyezes(Telefon cel)
        {
            if (hivasFigyelo != null)
                hivasFigyelo.KimenoHivasTortent(this, cel.Telefonszam); // jelzés az "eseményről" (történt valami)

            if (egyenleg >= HIVASDIJ)
            {
                cel.HivasFogadas(this);
                egyenleg -= HIVASDIJ;
            }
        }
    }
}
