﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Delegate_Alapok
{
    // delegált definiálása
    public delegate void SzovegKozvetito(string szoveg);

    public delegate double MuveletKozvetito(double szam);


    class Program
    {
        static void Main(string[] args)
        {
            // Metódushívás képviselőn (delegálton) keresztül:
            //   - példányt ebből is létre lehet hozni
            //   - ehhez a konstruktorának át kell adni a későbbiekben hívandó metódust
            //   - figyeljünk, hogy az átadott metódus (és a később feliratkoztatott metódusok) mind egyforma szignatúrával kell rendelkezzenek!
            //   - a metódus szignatúráját a delegált típusa írja elő (amit fent adtunk meg)

            SzovegKozvetito szovegFeldolgozo;

            // feliratkoztatás, "gyűjtőbe belerakás"
            szovegFeldolgozo = new SzovegKozvetito(KiirKicsi);
            szovegFeldolgozo += new SzovegKozvetito(KiirNagy);

            // a feliratkoztatott metódus(ok) meghívása delegálton keresztül
            // minden metódust meghív az adott paraméterrel
            szovegFeldolgozo("Teszt Üzenet");

            szovegFeldolgozo -= KiirNagy; // leiratkoztatás, "gyűjtőből kivétel"

            szovegFeldolgozo("Másik Teszt Üzenet");


            MuveletKozvetito muveletVegzo = null;
            muveletVegzo += Negyzet;  // így is lehet írni (feliratkoztatás, "gyűjtőbe belerakás")

            double pi = 3.14;
            double piNegyzet = muveletVegzo(pi);
            Console.WriteLine(piNegyzet);

            //szovegFeldolgozo += Negyzet; // hibás, nem egyezik a szignatúra

            Console.ReadLine();
        }

        static double Negyzet(double szam)
        {
            //return Math.Pow(szam, 2);
            return szam * szam;
        }


        static void KiirKicsi(string szoveg)
        {
            Console.WriteLine(szoveg.ToLower());
        }

        static void KiirNagy(string s)
        {
            Console.WriteLine(s.ToUpper());
        }
    }
}
