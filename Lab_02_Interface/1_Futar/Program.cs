﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Futar
{
    class Program
    {
        static void Main(string[] args)
        {
            // Interfész használata objektumokat összekötő "kapocsként"
            // => úgy cserélhetjük le a futárcéget (egy jobbra), hogy a Webshop osztályt nem kell hozzá módosítanunk !!!

            ChinaPost futar = new ChinaPost();
            DHLExpress dhlFutar = new DHLExpress();

            Webshop aliexpress;

            //aliexpress = new Webshop(futar);
            aliexpress = new Webshop(dhlFutar);

            aliexpress.Rendeles();

            Console.ReadLine();
        }

        public static void LogToConsole(string szoveg)
        {
            Console.WriteLine(szoveg);
        }
    }
}
