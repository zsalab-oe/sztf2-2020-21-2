﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Futar
{
    class DHLExpress : IFutarceg
    {
        public string Szallit()
        {
            Program.LogToConsole("[DHLExpress] - Express szállítás! Csomagja 5-7 nap múlva érkezik.");
            Random rnd = new Random();
            string TN = rnd.Next(1000000, 10000000).ToString() + "DHL";
            return TN;
        }
    }
}
