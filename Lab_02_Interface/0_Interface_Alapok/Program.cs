﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0_Interface_Alapok
{
    class Program
    {
        static void Main(string[] args)
        {
            Papagaj csorike = new Papagaj();
            csorike.Nev = "Csőrike";
            IHaziallat fonix = new Papagaj();  // interfész használható referencia típusként
            fonix.Nev = "Főnix";

            fonix.Eszik();

            //IHaziallat csipi = new IHaziallat(); // hibás, interfész típus nem példányosítható


            Macska csizmas = new Macska() { Nev = "Csizmás" };
            IHaziallat csizmasRef2 = csizmas;
            IVadasz csizmasRef3 = csizmas;
            //csizmas.
            //csizmasRef2.
            //csizmasRef3.


            IKedvencHaziallat zsomle = new Kutya() { Nev = "Zsömle" };

            zsomle.Setal("park");

            // interfész referencián keresztül csak az interfészben megadott metódusokat érjük el
            //zsomle.Ugat(); // így nem elérhető
            // de az IS és AS kulcsszavas varázslás itt is működik

            if (zsomle is Kutya)
                (zsomle as Kutya).Ugat();



            IHaziallat[] allatok = new IHaziallat[] { csorike, fonix, csizmas, zsomle };

            // polimorfizmus működik interfészek esetén is
            // hiszen ez esetben CSAK késői kötéssel működnek a metódusok
            // ha belegondolunk, korai kötés nem is működne, hiszen nincs az "ősben" (interfészben) kifejtve a metódus
            for (int i = 0; i < allatok.Length; i++)
            {
                // interfész referencián keresztül csak az interfészben megadott metódusokat érjük el
                allatok[i].Eszik();
            }


            Console.ReadLine();
        }
    }
}
