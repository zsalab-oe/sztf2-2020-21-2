﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0_Interface_Alapok
{
    interface IHaziallat
    {
        string Nev { get; set; }
        void Eszik();
        //int eletkor;// hibás, interfész nem tartalmazhat példányszintű mezőt

        // láthatósági jelzőket nem kell, és nem is lehet megadni (minden public)
        // nincs konkrét metódus, tulajdonság implementáció (metódustörzs)
        // megjegyzés: C# 8.0 verziótól kezdve ezekre is van lehetőség
    }

    interface IVadasz
    {
        void Vadaszik();
    }

    interface IKedvencHaziallat : IHaziallat
    {
        void Setal(string hol);
    }
}
