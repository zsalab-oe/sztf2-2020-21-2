﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    class Bankszamla
    {
        public string Tulajdonos { get; private set; }
        decimal egyenleg;
        public bool Aktiv { get; set; }

        public Bankszamla(string tulajdonos, int egyenleg)
        {
            Tulajdonos = tulajdonos;
            this.egyenleg = egyenleg;
            Aktiv = true;
        }

        public void PenzBefizetes(decimal osszeg)
        {
            if (!Aktiv)
            {
                throw new BankszamlaException(this);
            }

            egyenleg += osszeg;
        }

        public void PenzFelvetel(decimal osszeg)
        {
            // bonyolult előkészületek...
            SegedMetodus1(osszeg);
        }


        private void SegedMetodus1(decimal osszeg)
        {
            // további bonyolult előkészületek...
            SegedMetodus2(osszeg);
        }

        private void SegedMetodus2(decimal osszeg)
        {
            if (!Aktiv)
            {
                throw new BankszamlaException(this);
            }
            if (osszeg > this.egyenleg)
            {
                decimal hianyzik = osszeg - this.egyenleg;
                throw new PenzFelvetelException(hianyzik, this);
            }

            egyenleg -= osszeg;
        }
    }
}
