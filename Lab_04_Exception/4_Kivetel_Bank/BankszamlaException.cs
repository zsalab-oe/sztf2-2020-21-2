﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Kivetel_Bank
{
    // kivételtípus létrehozásánál érdemes elmenteni néhány - a kivételkezelés szempontjából hasznos - adatot, ezzel némi információt szolgáltatva majd a kivételt elkapónak / kezelőnek
    // tipikusan ilyen a kivételt okozó (problémás) érték, vagy objektum(ra egy referencia)

    class BankszamlaException : Exception
    {
        public Bankszamla Szamla { get; }

        public BankszamlaException(Bankszamla Szamla)
        {
            this.Szamla = Szamla;
        }

        public BankszamlaException(Bankszamla Szamla, string msg)
            : base(msg)
        {
            this.Szamla = Szamla;
        }
    }
}
