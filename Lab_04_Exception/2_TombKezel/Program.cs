﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_TombKezel
{
    class Program
    {
        static void Main(string[] args)
        {
            // - a program helyes (elvárt) működése és az esetleges hibák kezelése jól elkülönül egymástól
            // - a TombKezeles() metódus nincs megtördelve ellenőrzésekkel, hibakezelésekkel, csak az elvárt működést leíró kódot tartalmazza tömören, átláthatóan

            try
            {
                TombKezel();
            }
            catch (FormatException)
            {
                Console.WriteLine("HIBA - nem megfelelő formátum. (Számjegyeket írj!)");
            }
            catch (OverflowException)
            {
                Console.WriteLine("HIBA - túl nagy szám.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("HIBA - kiindexeltél a tömbből.");
            }


            Console.ReadLine();
        }

        static void TombKezel()
        {
            Console.WriteLine("Add meg a tömb méretét!");
            int[] tomb = new int[int.Parse(Console.ReadLine())];
            Console.WriteLine("Add meg a tömb elemeit!");
            for (int i = 0; i < tomb.Length; i++)
            {
                Console.Write((i + 1) + ". elem: ");
                tomb[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Hanyadik elemre vagy kíváncsi?");
            Console.WriteLine(tomb[int.Parse(Console.ReadLine()) - 1]);
        }
    }
}
