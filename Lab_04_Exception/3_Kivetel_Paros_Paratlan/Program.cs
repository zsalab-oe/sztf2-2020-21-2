﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Kivetel_Paros_Paratlan
{
    // Saját kivétel osztály
    // kötelezően az Exception osztály leszármazottja (enélkül nem lehet "hibát dobni")
    class ParatlanException : Exception
    {
        // kivételtípus létrehozásánál érdemes elmenteni néhány - a kivételkezelés szempontjából hasznos - adatot, ezzel némi információt szolgáltatva majd a kivételt elkapónak / kezelőnek
        // tipikusan ilyen a kivételt okozó (problémás) érték, vagy objektum(ra egy referencia)


        int szam;

        // örökölt, virtuális tulajdonság
        //public override string Message { get { return $"A megadott szám ({szam}) nem páros."; } }

        public ParatlanException()
        {

        }

        public ParatlanException(int szam)
        {
            this.szam = szam;
        }

        public ParatlanException(int szam, string message)
            : base(message)
        {
            this.szam = szam;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("x: ");
            int x = int.Parse(Console.ReadLine());

            try
            {
                int eredmeny = OsztasKettovel(x);
                Console.WriteLine("x/2 = " + eredmeny);
            }
            catch (ParatlanException e) // eldobott kivétel elkapása
            {
                //Console.WriteLine("Hiba, nem páros.");

                Console.WriteLine(e.Message);
            }



            Console.ReadLine();
        }

        static int OsztasKettovel(int szam)
        {
            if (szam % 2 == 0)
            {
                return szam / 2;
            }
            else
            {
                // új kivételOBJEKTUM eldobása:

                //throw new ParatlanException();

                //throw new ParatlanException(szam);

                throw new ParatlanException(szam, $"A megadott szám ({szam}) páratlan, nem osztható kettővel.");
            }
        }
    }
}
